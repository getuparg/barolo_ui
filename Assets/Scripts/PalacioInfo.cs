﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using SimpleJSON;
using System;

public class PalacioInfo : MonoBehaviour
{
    [SerializeField] private string url;

    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private Text info;

    [SerializeField] private string[] titles;
    [SerializeField] private string[] infos;

    [SerializeField] private float maxSize;
    [SerializeField] private float minSize;

    private void Awake()
    {
        StartCoroutine(GetInformativeTextsRoutine(OnInformativeTextsComplete));
    }

    public void UpdateText(int index)
    {
        if (titles.Length > 0 && infos.Length > 0)
        {
            title.text = titles[index];
            info.text = infos[index];
        }
    }

    public void SetOnInitial()
    {
        title.text = titles[0];
        info.text = infos[0];
    }

    IEnumerator GetInformativeTextsRoutine(Action<InformativeText[]> onComplete)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {

            Debug.Log("[INFORMATIVE TEXTS]");
            Debug.Log(www.downloadHandler.text);

            var jsonObject = JSON.Parse(www.downloadHandler.text);
            Debug.Log("COunt: " + jsonObject["data"].AsArray.Count);
            InformativeText[] events = new InformativeText[jsonObject["data"].AsArray.Count];

            for (int i = 0; i < jsonObject["data"].AsArray.Count; i++)
            {
                Debug.Log("Entro aca ?");
                InformativeText newInformativeText = new InformativeText();
                newInformativeText.id = jsonObject["data"].AsArray[i]["id"].AsInt;
                newInformativeText.type = jsonObject["data"].AsArray[i]["type"];
                newInformativeText.title = jsonObject["data"].AsArray[i]["title"];
                newInformativeText.info = jsonObject["data"].AsArray[i]["info"];
                events[i] = newInformativeText;
            }

            onComplete(events);
        }
    }

    private void OnInformativeTextsComplete(InformativeText[] events)
    {
        Debug.Log("events size: " + events.Length);

        titles = new string[events.Length];
        infos = new string[events.Length];

        for (int i = 0; i < events.Length; i++)
        {
            titles[i] = events[i].title;
            infos[i] = events[i].info.Replace("\n", String.Empty);
        }
        SetOnInitial();
    }
}

public class InformativeText
{
    public int id;
    public string type;
    public string title;
    public string info;
}