[System.Serializable]
public class TourismEvent
{
    public int id;
    public string type;
    public string title;
    public string info;
    public string link;
    public string imageURL;
}