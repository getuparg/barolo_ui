﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using System;

public class TourismController : MonoBehaviour
{
    [SerializeField] private string url;

    private TourismCanvasController tourismCanvasController;

    private void Awake() 
    {
        tourismCanvasController = FindObjectOfType<TourismCanvasController>();    
    }

    public void GetTourismEvents()
    {
        StartCoroutine(GetTourismEventsRoutine(OnTourismEventsComplete));
    }

    public void GetNewsEvents()
    {
        StartCoroutine(GetTourismEventsRoutine(OnNewsEventsComplete));
    }

    public void GetHomeEvents()
    {
        StartCoroutine(GetTourismEventsRoutine(OnHomeEventsComplete));
    }

    public void GetVisitasEvents()
    {
        StartCoroutine(GetTourismEventsRoutine(OnVisitasEventsComplete));
    }

    public void GetEvents()
    {
        StartCoroutine(GetTourismEventsRoutine(OnEventsComplete));
    }

    IEnumerator GetTourismEventsRoutine(Action<TourismEvent[]> onComplete)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            
            Debug.Log("[EVENTS]");
            //Debug.Log(www.downloadHandler.text);

            var jsonObject = JSON.Parse(www.downloadHandler.text);
            
            TourismEvent[] events = new TourismEvent[jsonObject["data"].AsArray.Count];
            
            for (int i = 0 ; i < jsonObject["data"].AsArray.Count; i++)
            {
                TourismEvent newEvent = new TourismEvent();
                newEvent.id = jsonObject["data"].AsArray[i]["id"].AsInt;
                newEvent.type = jsonObject["data"].AsArray[i]["type"];
                newEvent.title = jsonObject["data"].AsArray[i]["titulo"];
                newEvent.info = jsonObject["data"].AsArray[i]["info"];
                newEvent.link = jsonObject["data"].AsArray[i]["link"];
                newEvent.imageURL = jsonObject["data"].AsArray[i]["imageUrl"];
                events[i] = newEvent;
            }

            onComplete(events);
        }
    }

    private void OnTourismEventsComplete(TourismEvent[] events)
    {
        tourismCanvasController.CreateTourismEvents(events, "Turismo");
    }

    private void OnNewsEventsComplete(TourismEvent[] events)
    {
        tourismCanvasController.CreateTourismEvents(events, "Noticias");
    }

    private void OnHomeEventsComplete(TourismEvent[] events)
    {
        tourismCanvasController.CreateTourismEvents(events, "Home");
    }

    private void OnVisitasEventsComplete(TourismEvent[] events)
    {
        tourismCanvasController.CreateTourismEvents(events, "Visitas Guiadas");
    }

    private void OnEventsComplete(TourismEvent[] events)
    {
        tourismCanvasController.CreateTourismEvents(events, "Eventos");
    }
}