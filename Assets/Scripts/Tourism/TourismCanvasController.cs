﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TourismCanvasController : MonoBehaviour
{

    public PrefabsConfig[] configs;

    public void CreateTourismEvents(TourismEvent[] events, string typeOfEvent)
    {
        var tourismEvents = events.Where(item => item.type == typeOfEvent).ToArray();

        var eventsConfig = configs.Where(cfg => cfg.type == typeOfEvent).ToArray()[0];

        int childs = eventsConfig.eventParent.childCount;

        // if(childs > 0)
        // {
        //     for(int i = 0; i < childs; i++)
        //     {
        //         Destroy(eventsConfig.eventParent.GetChild(i).gameObject);
        //     }            
        // }
        if (childs == 0)
        {
            for (int i = 0; i < tourismEvents.Length; i++)
            {
                GameObject g = Instantiate(eventsConfig.eventPrefab, eventsConfig.eventParent);
                TourismEventCanvas t = g.GetComponent<TourismEventCanvas>();
                t.SetData(tourismEvents[i].id, tourismEvents[i].title, tourismEvents[i].info, tourismEvents[i].link, tourismEvents[i].imageURL);
                t.StartCoroutine(t.LoadImage(tourismEvents[i].imageURL, t.ReplaceImage));
            }
        }
        else
        {
            for (int i = 0; i < eventsConfig.eventParent.childCount; i++)
            {

                TourismEventCanvas t = eventsConfig.eventParent.GetChild(i).GetComponent<TourismEventCanvas>();
                if (!t.completeDownload)
                {
                    t.StartCoroutine(t.LoadImage(tourismEvents[i].imageURL, t.ReplaceImage));
                }
            }
        }

    }

}

[System.Serializable]
public class PrefabsConfig
{
    public string type;
    public GameObject eventPrefab;
    public Transform eventParent;
}
