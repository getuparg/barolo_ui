﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using TMPro;

public class BaroloCartProduct : MonoBehaviour
{
    [SerializeField] private string id;
    [SerializeField] private string imageUrl;
    [SerializeField] private UnityEngine.UI.Image image;
    [SerializeField] private TextMeshProUGUI prodName;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private TextMeshProUGUI price;
    [SerializeField] private TextMeshProUGUI currentQuantity;
    [SerializeField] private GameObject loading;
    public Button increaseQtyButton;
    public Button decreaseQtyButton;

    private void Update()
    {
        if (this.isActiveAndEnabled)
        {
            if (imageUrl != "")
            {
                StartCoroutine(LoadImage(imageUrl, ReplaceImage));
            }
        }
    }

    public void Init(string id, string imageUrl, string prodName, string description, string price)
    {
        this.id = id;
        this.prodName.text = prodName;
        this.description.text = description;
        this.price.text = price;
        this.imageUrl = imageUrl;
    }

    public string GetID()
    {
        return this.id;
    }

    public void SetQuantity(long quantity)
    {
        this.currentQuantity.text = quantity.ToString();
    }

    public IEnumerator LoadImage(string path, Action<Texture2D> onComplete)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                var texture = DownloadHandlerTexture.GetContent(uwr);
                onComplete(texture);
            }

        }
    }

    public void ReplaceImage(Texture2D texture)
    {
        Rect rect = new Rect(0f, 0f, texture.width, texture.height);
        image.sprite = Sprite.Create(texture, rect, Vector2.zero);
        loading.SetActive(false);
    }
}
