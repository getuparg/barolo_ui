﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shopify.Unity;
using Shopify.Unity.SDK;
using System.Linq;
using TMPro;

public class BaroloShop : MonoBehaviour
{
    [SerializeField] string accessToken;
    [SerializeField] string shopDomain;

    [SerializeField] GameObject shopPanel;
    [SerializeField] GameObject cartPanel;
    [SerializeField] TextMeshProUGUI subPanelTitle;

    [Header("Shop")]
    [SerializeField] GameObject productPrefab;
    [SerializeField] Transform productParent;

    [Header("Cart")]
    [SerializeField] GameObject cartProductPrefab;
    [SerializeField] Transform cartProductParent;

    [SerializeField] TextMeshProUGUI cartProductsQty;

    private Cart cart;

    List<GameObject> gameobjectsProducts = new List<GameObject>();
    List<GameObject> gameobjectsCartProducts = new List<GameObject>();

    void Start()
    {
        // Init only needs to be called once
        ShopifyBuy.Init(accessToken, shopDomain);

        CreateCart();
        FetchProducts();
    }

    private void CreateCart()
    {
        cart = ShopifyBuy.Client().Cart();
    }

    private void FetchProducts()
    {
        ShopifyBuy.Client().products((products, error, after) =>
        {
            if (error != null)
            {
                Debug.Log(error.Description);

                switch (error.Type)
                {
                    // An HTTP error is actually Unity's WWW.error
                    case ShopifyError.ErrorType.HTTP:
                        break;
                    // Although it's unlikely, an invalid GraphQL query might be sent.
                    // Report an issue to https://github.com/shopify/unity-buy-sdk/issues
                    case ShopifyError.ErrorType.GraphQL:
                        break;
                };
            }
            else
            {
                Debug.Log("Here is the first page of products:");

                // products is a List<Product>
                foreach (Product product in products)
                {
                    GameObject newProduct = Instantiate(productPrefab, productParent);
                    BaroloProduct baroloProd = newProduct.GetComponent<BaroloProduct>();

                    var variants = (List<Shopify.Unity.ProductVariant>)product.variants();
                    var images = (List<Shopify.Unity.Image>)product.images();

                    baroloProd.Init(product.id(), images.First().transformedSrc("compact"), product.title(), product.description(), variants.First().price().ToString());
                    baroloProd.addToCartButton.onClick.AddListener(() => AddProductToCart(product, variants.First(), 1));

                    gameobjectsProducts.Add(newProduct);

                    Debug.Log("Product Title: " + product.title());
                    Debug.Log("Product Description: " + product.descriptionHtml());
                    Debug.Log("--------");
                }

                if (after != null)
                {
                    Debug.Log("Here is the second page of products:");

                    // Queries second page of products, as after is passed
                    ShopifyBuy.Client().products((products2, error2, after2) =>
                    {
                        foreach (Product product in products2)
                        {
                            Debug.Log("Product Title: " + product.title());
                            Debug.Log("Product Description: " + product.descriptionHtml());
                            Debug.Log("--------");
                        }
                    }, after: after);
                }
                else
                {
                    Debug.Log("There was only one page of products.");
                }
            }
        });
    }

    public void AddProductToCart(Product product, ProductVariant productToAdd, int quantity = 1)
    {
        if (cart != null && productToAdd.availableForSale())
        {
            cart.LineItems.AddOrUpdate(productToAdd, quantity);
        }

        UpdateCartQuantity();
        UpdateCartProducts(product, productToAdd);
    }

    public void DeleteProductInCart(Product product, ProductVariant productToDelete)
    {
        if (cart != null)
        {
            cart.LineItems.Delete(productToDelete);
        }

        UpdateCartQuantity();
    }

    public void UpdateProductQuantity(Product product, ProductVariant productToUpdate, int quantity = 1)
    {
        if (cart != null)
        {
            cart.LineItems.AddOrUpdate(productToUpdate, 1);
        }

        UpdateCartQuantity();
        UpdateCartProducts(product, productToUpdate);
    }

    public void GoCartPanel()
    {
        shopPanel.SetActive(false);
        cartPanel.SetActive(true);
        subPanelTitle.gameObject.SetActive(true);
    }

    public void GoShopPanel()
    {
        shopPanel.SetActive(true);
        cartPanel.SetActive(false);
        subPanelTitle.gameObject.SetActive(false);
    }

    private void UpdateCartQuantity()
    {
        if (cart.LineItems.All().Count > 0)
        {
            cartProductsQty.text = cart.LineItems.All().Count.ToString();
            cartProductsQty.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            cartProductsQty.text = "";
            cartProductsQty.transform.parent.gameObject.SetActive(false);
        }
    }

    private void UpdateCartProducts(Product productToAdd, ProductVariant productVariantToAdd)
    {
        if (!CartProductExist(productToAdd))
        {
            GameObject newCartObject = Instantiate(cartProductPrefab, cartProductParent);
            BaroloCartProduct cartProduct = newCartObject.GetComponent<BaroloCartProduct>();
            cartProduct.Init(productToAdd.id(), productVariantToAdd.image().transformedSrc("compact"), productToAdd.title(), productToAdd.description(), productVariantToAdd.price().ToString());
            cartProduct.increaseQtyButton.onClick.AddListener(() => UpdateProductQuantity(productToAdd, cartProduct, productVariantToAdd, 1));
            cartProduct.decreaseQtyButton.onClick.AddListener(() => UpdateProductQuantity(productToAdd, cartProduct, productVariantToAdd, -1));
            gameobjectsCartProducts.Add(newCartObject);
        }

    }

    private bool CartProductExist(Product product)
    {
        Debug.Log("Count cart prods: " + gameobjectsCartProducts.Count);
        foreach (GameObject g in gameobjectsCartProducts)
        {
            BaroloCartProduct b = g.GetComponent<BaroloCartProduct>();
            if (b.GetID() == product.id())
                return true;
        }
        return false;
    }

    private void UpdateProductQuantity(Product product, BaroloCartProduct cartProduct, ProductVariant productToUpdate, int quantityAdjustment)
    {
        var lineItem = cart.LineItems.Get(productToUpdate);
        if (cart != null)
        {
            cart.LineItems.AddOrUpdate(productToUpdate, lineItem.Quantity + quantityAdjustment);
        }

        if (lineItem.Quantity <= 0)
        {
            Destroy(cartProduct.gameObject);
            gameobjectsCartProducts.Remove(cartProduct.gameObject);

            DeleteProductInCart(product, productToUpdate);

            if (cart.LineItems.All().Count <= 0)
            {
                GoShopPanel();
            }
        }
        else
        {
            cartProduct.SetQuantity(lineItem.Quantity);
        }

        UpdateCartQuantity();

    }

    public void CartCheckout()
    {

        if (cart != null && cart.LineItems.All().Count > 0)
        {
            cart.CheckoutWithWebView(
        success: () =>
        {
            Debug.Log("User finished purchase/checkout!");
        },
        cancelled: () =>
        {
            Debug.Log("User cancelled out of the web checkout.");
        },
        failure: (e) =>
        {
            Debug.Log("Something bad happened - Error: " + e);
        }
            );
        }

        // cart.GetWebCheckoutLink(
        //     success: (link) => {
        //         Application.OpenURL(link);
        //     },
        //     failure: (checkoutError) => {
        //         Debug.Log(checkoutError.Description);
        //     }
        // );

    }




}
