﻿using System.Collections;
using UnityEngine;
using System;

public class AutoCarousel : MonoBehaviour
{
    [SerializeField] private Transform rect;
    [SerializeField] private float distanceToMovePerImage;
    [SerializeField] private ImageCarousel imageCarousel; 
    [SerializeField] private float velocity; 

    [SerializeField] private bool moveCarousel;
    
    private int count = 1;
    private float lerpTime = 0;
    private Vector2 newPos;
    private bool courotineNotWorking = false;
    

    private void Awake()
    {
        moveCarousel = false;
    }


    private void Update()
    {
        if (moveCarousel)
        {
            StartCoroutine(MoveCarouselRoutine());
        }
    }

    IEnumerator MoveCarouselRoutine()
    {

        if (!courotineNotWorking)
        {
            courotineNotWorking = true;
            float time = 0;
            float time2 = 0;
            yield return new WaitForSeconds(1f);

            while (time < 1)
            {
                time += velocity;
                rect.position = new Vector3(Mathf.Lerp(0, -1080f , time), rect.position.y, 0);
                yield return new WaitForSeconds(velocity);
                imageCarousel.DebugScroll(rect.position);
            }

            yield return new WaitForSeconds(1f);
            
            while (time2 < 1)
            {
                time2 += velocity;
                rect.position = new Vector3(Mathf.Lerp(-1080f, -2160f, time2), rect.position.y, 0);
                yield return new WaitForSeconds(velocity);
                imageCarousel.DebugScroll(rect.position);
            }

            yield return new WaitForSeconds(1f);
            
            rect.position = new Vector3(0,rect.position.y,0);
            imageCarousel.DebugScroll(rect.position);
            
            courotineNotWorking = false;
        }
    }

    public void PlayCarousel()
    {
        moveCarousel = true;
    }

    public void StopCarousel()
    {
        moveCarousel = false;
    }

    public void ResetCarousel()
    {
        moveCarousel = false;
        StopAllCoroutines();
        courotineNotWorking = false;
        rect.position = Vector3.zero;
    }

    public bool IsMoving()
    {
        return moveCarousel;
    }

    public bool IsReadyToMove()
    {
        return rect.childCount > 0;
    }

}
