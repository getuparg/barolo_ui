﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayStopButton : MonoBehaviour
{
    [SerializeField] private GameObject playBtnSprite;
    [SerializeField] private VideoPlayer video = null;

    private void PlayStop()
    {
        if(video != null && video.isPlaying)
        {
            video.Pause();
            video.GetTargetAudioSource(0).Pause();
            playBtnSprite.SetActive(true);
        }
        else if(video.isPaused)
        {
            video.Play();
            video.GetTargetAudioSource(0).Play();
            playBtnSprite.SetActive(false);
        }
        else
        {
            Debug.Log("Not video on parent.");
        }
    }

    private void OnMouseDown() 
    {
        PlayStop();
    }
}
