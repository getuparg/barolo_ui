﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class BaroloExperience : MonoBehaviour
{
    private Animator anim = null;
    private VideoPlayer video = null;
    private AudioSource audio = null;

    private void Awake() 
    {
        anim = GetComponent<Animator>();    
        video = GetComponentInChildren<VideoPlayer>();  
        audio = GetComponentInChildren<AudioSource>();  
    }

    public void OnTrackerFound()
    {
        if (anim != null)
        {
            anim.SetBool("Found",true);
        }

        if(video !=  null)
        {
            video.Play();

            if(audio != null)
            {
                audio.Play();
            }
        }
    }

    public void OnTrackerLost()
    {
        if (anim != null)
        {
            anim.SetBool("Found",false);
        }

        if(video !=  null)
        {
            video.Pause();

            if(audio != null)
            {
                audio.Pause();
            }
            
        }
    }
}
