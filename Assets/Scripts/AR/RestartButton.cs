﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class RestartButton : MonoBehaviour
{
    [SerializeField] private VideoPlayer video;

    private void Restart()
    {
        if(video != null)
        {
            video.Stop();
            video.GetTargetAudioSource(0).Stop();
            video.Play();
            video.GetTargetAudioSource(0).Play();
        }
        else
        {
            Debug.Log("No video player");
        }
    }

    private void OnMouseDown() 
    {
        Restart();
    }
}
