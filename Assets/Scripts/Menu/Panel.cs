﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Panel : MonoBehaviour
{
    [SerializeField] private string id;
    [SerializeField] private GameObject[] subPanels;
    [SerializeField] private Image[] selectedImages;
    [SerializeField] private Sprite activeImage;
    [SerializeField] private Sprite inactiveImage;
    [SerializeField] private Button selectedButton;

    private void Awake()
    {
        StartCoroutine(SelectButtonInitial());    
    }

    private void ResetSubPanels()
    {
        foreach (GameObject g in subPanels)
        {
            g.SetActive(false);
        }

    }

    public void GoSubPanel(int index)
    {
        ResetSubPanels();
        subPanels[index].SetActive(true);
    }

    private void ResetSelectedImages()
    {
        foreach (Image i in selectedImages)
        {
            i.overrideSprite = inactiveImage;
        }
    }

    public void ImageSelected(Vector2 vector)
    {
        ResetSelectedImages();

        if (vector.x <= 0.5f)
        {
            selectedImages[0].overrideSprite = activeImage;
        }
        else if ((vector.x >= 0.5f) && (vector.x < 1f))
        {
            selectedImages[1].overrideSprite = activeImage;
        }
        else if (vector.x >= 1f)
        {
            selectedImages[2].overrideSprite = activeImage;
        }
    }

    IEnumerator SelectButtonInitial()
     {
         yield return null;
         selectedButton.OnSelect(null);
         selectedButton.Select();
     }

}
