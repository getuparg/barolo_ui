﻿using UnityEngine;
using UnityEngine.UI;

public class SubButtonWithSelection : MonoBehaviour
{
    [SerializeField] private Sprite activeSprite;
    [SerializeField] private Sprite inActiveSprite;

    public bool activeAtStart;

    [SerializeField] private Image btnImage;

    [SerializeField] private SubButtonWithSelection[] brothers;

    public void SelectBtn() 
    {

        foreach(SubButtonWithSelection s in brothers)
        {
            s.SetInactive();
        }

        if(btnImage.sprite != activeSprite)
        {
            btnImage.sprite = activeSprite;    
        }
        else
        {
            btnImage.sprite = inActiveSprite;    
        }
        
    }

    public void SetInactive()
    {
        btnImage.sprite = inActiveSprite;
    }

    public void SetActive()
    {
        btnImage.sprite = activeSprite;
    }

}
