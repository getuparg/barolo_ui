﻿using System.Collections.Generic;
using UnityEngine;

public class ButtonWithSelection : MonoBehaviour
{

    [SerializeField] private GameObject selectedSprite;
    [SerializeField] private GameObject noSelectedSprite;
    
    [SerializeField] private List<ButtonWithSelection> brothers = new List<ButtonWithSelection>();
    
    public bool activeAtStart = false;
    public bool inactiveBrothers = false;

    public void SelectBtn() 
    {
        // if(selectedSprite.activeSelf)
        // {
        //     selectedSprite.SetActive(false);
        //     noSelectedSprite.SetActive(true);
        // }
        // else
        // {
        //     selectedSprite.SetActive(true);
        //     noSelectedSprite.SetActive(false);
        // }
        SetActive();

        if(inactiveBrothers)
        {
            foreach (ButtonWithSelection button in brothers)
            {
                button.SetInactive();   
            }
        }
    }

    public void SetInactive()
    {
        selectedSprite.SetActive(false);
        noSelectedSprite.SetActive(true);
    }

    public void SetActive()
    {
        selectedSprite.SetActive(true);
        noSelectedSprite.SetActive(false);
    }

}
