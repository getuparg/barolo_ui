﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PanelController : MonoBehaviour
{
    [Header("Panels")]

    [SerializeField] private GameObject homePanel;
    [SerializeField] private GameObject palacioPanel;
    [SerializeField] private GameObject infoPanel;
    [SerializeField] private GameObject tourismPanel;
    [SerializeField] private GameObject arPanel;
    [SerializeField] private GameObject shopPanel;

    [Header("Buttons")]

    [SerializeField] private ButtonWithSelection[] buttonsWithSelections; 
    [SerializeField] private ButtonWithSelection[] buttonsWithSelectionsPalacioInfo; 

    [Header("Texts")]
    
    [SerializeField] private TextMeshProUGUI headerText;

    [Header("Controllers")]

    TourismController tourismContrller;
    [SerializeField] PalacioInfo palacioInfo;
    [SerializeField] AutoCarousel autoCarousel;

    private void Awake() 
    {
        tourismContrller = FindObjectOfType<TourismController>();
        //news are at the start panel
        tourismContrller.GetNewsEvents();
        tourismContrller.GetHomeEvents();
    }

    private void Update() 
    {
        if(!homePanel.activeInHierarchy && autoCarousel.IsMoving())
        {
            autoCarousel.ResetCarousel();
        }

        if(homePanel.activeInHierarchy && autoCarousel.IsReadyToMove() && !autoCarousel.IsMoving())
        {
            autoCarousel.PlayCarousel();
        }
    }

    private void ResetPanels()
    {
        homePanel.SetActive(false);
        palacioPanel.SetActive(false);
        infoPanel.SetActive(false);
        tourismPanel.SetActive(false);
        arPanel.SetActive(false);
        shopPanel.SetActive(false);
    }

    private void ResetButtonsSprite()
    {
        foreach(ButtonWithSelection b in buttonsWithSelections)
        {
            b.SetInactive();
        }

        foreach(ButtonWithSelection p in buttonsWithSelectionsPalacioInfo)
        {
            if(p.activeAtStart)
            {
                p.SetActive();
            }
            else
            {
                p.SetInactive();
            }

        }

    }

    public void GoHome()
    {
        ResetPanels();
        ResetButtonsSprite(); 

        homePanel.SetActive(true);

        tourismContrller.GetHomeEvents();
        tourismContrller.GetNewsEvents();

        autoCarousel.PlayCarousel();
    }

    public void GoPalacio()
    {
        ResetPanels();
        ResetButtonsSprite();

        palacioPanel.SetActive(true);
        
        headerText.text = "EL PALACIO";
        //palacioInfo.SetOnInitial();
    }

    public void GoInfo()
    {   
        
        ResetPanels();
        ResetButtonsSprite();

        infoPanel.SetActive(true);
                
        headerText.text = "INFORMACION";

        tourismContrller.GetVisitasEvents();
    }

    public void GoEvents()
    {
        tourismContrller.GetEvents();
    }

    public void GoTourism()
    {
        ResetPanels();
        ResetButtonsSprite();

        tourismPanel.SetActive(true);

        headerText.text = "TURISMO";

        tourismContrller.GetTourismEvents();
    }

    public void GoShop()
    {
        ResetPanels();
        ResetButtonsSprite();

        shopPanel.SetActive(true);

        headerText.text = "SHOP";
    }

    public void GoScene(string scene) 
    {
        SceneManager.LoadScene(scene);
    }



}
