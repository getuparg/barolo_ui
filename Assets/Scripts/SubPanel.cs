﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SubPanel : MonoBehaviour
{
    [SerializeField] private GameObject[] subPanels;
    [SerializeField] private Image[] selectedImages;

    private void ResetSubPanels()
    {
        foreach (GameObject g in subPanels)
        {
            g.SetActive(false);
        }

    }

    public void GoSubPanel(int index)
    {
        ResetSubPanels();
        subPanels[index].SetActive(true);
    }

    private void ResetSelectedImages()
    {
        // foreach (Image i in selectedImages)
        // {
        //     i.overrideSprite = inactiveImage;
        // }
    }
    
}
