﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using System;
using UnityEngine.Networking;

public class TourismEventCanvas : MonoBehaviour
{
    public TextMeshProUGUI title;
    public TextMeshProUGUI info;
    public TextMeshProUGUI link;
    public Image image;
    public GameObject loading;

    private TourismEvent data;
    public bool completeDownload { get; private set; }

    private void OnEnable() 
    {
        completeDownload = false;
    }

    public IEnumerator LoadImage(string path, Action<Texture2D> onComplete)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                var texture = DownloadHandlerTexture.GetContent(uwr);
                onComplete(texture);
            }
        }
    }

    public void ReplaceImage(Texture2D texture)
    {
        Rect rect = new Rect(0f, 0f, texture.width, texture.height);
        image.sprite = Sprite.Create(texture, rect, Vector2.zero);
        image.preserveAspect = true;
        loading.SetActive(false);
        completeDownload = true;
    }

    public void SetData(int id, string title, string info, string link, string imageUrl)
    {
        data = new TourismEvent();
        data.id = id;
        data.title = title;
        data.info = info;
        data.link = link;
        data.imageURL = imageUrl;

        LoadData();
    }

    public void OnClickEvent()
    {
        if (data.link != "")
        {
            Application.OpenURL(data.link);
        }
    }

    private void LoadData()
    {
        if (this.title != null)
        {
            this.title.text = data.title;
        }

        if (this.info != null)
        {
            this.info.text = data.info;
        }

        if (this.link != null)
        {
            this.link.text = data.link;
        }

    }

}
