﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ImageCarousel : MonoBehaviour
{
    [SerializeField] private Image[] images;

    [SerializeField] private Sprite active;
    [SerializeField] private Sprite inActive;

    private void Reset() 
    {
        foreach(Image i in images)
        {
            i.sprite = inActive;
        }
    }

    public void DebugScroll(Vector3 vector)
    {
        if(vector.x >= 0)
        {
            images[0].sprite = active;
            images[1].sprite = inActive;
            images[2].sprite = inActive;
        } else if(vector.x >= -1080 && vector.x < 0)
        {
            images[0].sprite = inActive;
            images[1].sprite = active;
            images[2].sprite = inActive;
        } else if(vector.x >= -2160f && vector.x < -1080f)
        {
            images[0].sprite = inActive;
            images[1].sprite = inActive;
            images[2].sprite = active;
        }


    }
    
}
